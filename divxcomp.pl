#!/usr/bin/perl -w

#############################################################################
# Copyright (C) 2002 Marc Leeman <mleeman@users.sourceforge.net>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#############################################################################

use Tk;
use Tk::BrowseEntry;

my $Version = "0.1";

sub divxcompute;

my $main = MainWindow->new();
$main->configure(-title=>"Calculate DivX bitrate $Version", -background=>'grey');
$main->geometry('+100+300');

my $top = $main->Frame(
                        )->pack(-expand=>1,-side => 'top', -fill => 'both');

my $cdlength = $top->Frame(
			)->pack(-expand=>1,-side => 'top', -fill => 'both');

my $movielength = $top->Frame(
			)->pack(-expand=>1,-side => 'top', -fill => 'both');
			
my $mp3bitrate = $top->Frame(
			)->pack(-expand=>1,-side => 'top', -fill => 'both');

my $result= $top->Frame(
			)->pack(-expand=>1,-side => 'top', -fill => 'both');

my $buttons = $top->Frame(
			)->pack(-expand=>1,-side => 'top', -fill => 'both');

my $cdlengthlabel = $cdlength->Label(-text => 'CD Length/File Size',
					)->pack(-side=>'left');

my $cdlengthentry = $cdlength->Entry(-width=>10,
					-background => 'white',
					-relief=>'sunken',
					)->pack(-side=>'right');

$cdlengthentry->insert('end',"700");

my $movielengthlabel = $movielength->Label(-text => 'DVD Movie Length (min)',
					)->pack(-side=>'left');

my $movielengthentry = $movielength->Entry(-width=>10,
					-background => 'white',
					-relief=>'sunken',
					)->pack(-side=>'right');
$movielengthentry->insert('end',"120");					

		
my $mp3bitrateentry = $mp3bitrate->BrowseEntry(-label => 'MP3 bitrate (kbps)',
				-choices => [qw(32 40 48 56 64 80 96 112 128 160 192 224 256 320)],
				-listwidth => 25,
				-variable => \$kbps)->pack();

my $resultFixed = $result->Label(-text => 'DivX bitrate',
                                        )->pack(-side=>'left');

my $bitrate = $result->Label(-text => 'undefined',
                                        )->pack(-side=>'right');

my $compute= $buttons->Button(-text=>'Calculate',
				-command=>\&divxcompute,
				)->pack(-side=>'left');

my $end = $buttons->Button(-text=>'Close',
				-command=>sub{exit(0)},
				)->pack(-side=>'right');


sub divxcompute{
	$ml = $movielengthentry->get();
	$cdl = $cdlengthentry->get();
	my $final = (((($cdl*1024*8)/1000)*1024)-($kbps*$ml*60))/($ml*60);
	
	if($final =~ /(^\d+)(:?.\d+)?/){
		$bitrate->configure(-text=>"$1");
	}
	else{
		$bitrate->configure(-text=>"Illegal Result");
	}
};

MainLoop();
